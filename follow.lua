
local M = {}

local PVector = require("PVector")

local function angleBetween( srcX, srcY, dstX, dstY )
	local angle = ( math.deg( math.atan2( dstY-srcY, dstX-srcX ) )+90 )
	return angle % 360
end

local function distBetween( x1, y1, x2, y2 )
	local xFactor = x2 - x1
	local yFactor = y2 - y1
	local dist = math.sqrt( (xFactor*xFactor) + (yFactor*yFactor) )
	return dist
end

local function smoothTransition(target, options)
    local desired = PVector.new({x = options.x - target.x, y = options.y - target.y})
    local d = desired:meg()
    local offset = d / options.time * (1000 / display.fps)
    desired:normalize()
    desired:multi(offset)
    local offsetX = desired.x
    local offsetY = desired.y
    --print("OffsetX: "..offsetX..", OffsetY:"..offsetY)

	local trans = nil

	trans = function()
		if target.x == nil then
			Runtime:removeEventListener( "enterFrame", trans )
			options.onComplete()
			print("the object is removed")
			return
		end
		
		local tx = math.floor(target.x)
		local ty = math.floor(target.y)
		local ox = math.floor(options.x)
		local oy = math.floor(options.y)

		--print("target.x: "..tx..", target.y: "..ty..", options.x: "..ox..", options.y: "..oy)

	    if tx ~= ox then
	        if target.x < options.x and target.x + offsetX > options.x then
	            target.x = options.x
	        elseif target.x > options.x and target.x + offsetX < options.x then
	            target.x = options.x
	        else
	            target.x = target.x + offsetX
	        end
	    end
	    
	    if ty ~= oy then
	        if target.y < options.y and target.y + offsetY > options.y then
	            target.y = options.y
	        elseif target.y > options.y and target.y + offsetY < options.y then
	            target.y = options.y
	        else
	            target.y = target.y + offsetY   
	        end 
	    end

	    if tx ==ox and ty == oy then
	    	Runtime:removeEventListener( "enterFrame", trans )
			--print("Complete")
			options.onComplete()
	    	return
	    end

	end
    Runtime:addEventListener( "enterFrame", trans )
end



local function follow( params, obj, pathPoints, pathPrecision )

	local function nextTransition()
		if obj.x == nil then
			--print("The object os removed")
			return 
		end
		if ( obj.nextPoint > #pathPoints ) then
			--print( "FINISHED" )

		else
			--set variable for time of transition on this segment
			local transTime = params.segmentTime or 0
			--if "params.constantRate" is true, adjust time according to segment distance
			if params.speed then
				local dist = distBetween( obj.x, obj.y, pathPoints[obj.nextPoint].x, pathPoints[obj.nextPoint].y )
				--print("Dist = "..dist..", time = "..(dist/params.speed*1000)..", speed: "..params.speed)
				transTime = (dist/params.speed*1000)
			elseif ( params.constantRate == true ) then
				local dist = distBetween( obj.x, obj.y, pathPoints[obj.nextPoint].x, pathPoints[obj.nextPoint].y )
				--print("Seg len = "..(dist/pathPrecision)..", pathPrecision: "..pathPrecision)
				--transTime = (dist/pathPrecision) * params.segmentTime
			end
			
			--rotate object to face next point
			if ( obj.nextPoint < #pathPoints ) then
				obj.rotation = angleBetween( obj.x, obj.y, pathPoints[obj.nextPoint].x, pathPoints[obj.nextPoint].y )
			end
			--merge transition
			--print("trans with time: "..transTime)
			if transTime >= (1000 / display.fps) then
				--transition along segment
				--transition.to( obj, {
				smoothTransition( obj, {
					tag = "moveObject",
					time = transTime,
					x = pathPoints[obj.nextPoint].x,
					y = pathPoints[obj.nextPoint].y,
					onComplete = nextTransition
				})
				obj.nextPoint = obj.nextPoint+1
			else
				--print("merge")
				obj.nextPoint = obj.nextPoint+1
				if obj.nextPoint > #pathPoints  then
					--print("end")
					obj.x = pathPoints[obj.nextPoint - 1].x
					obj.y = pathPoints[obj.nextPoint - 1].y
				end
				nextTransition()
			end

		end
	end
	
	obj.nextPoint = 2
	nextTransition()
end



function M.init( params, pathPoints, pathPrecision, startPoint )

	local follower = display.newPolygon( 0, 0, { 0,-28, 30,28, 0,20, -30,28 } )
	follower:setFillColor( 1 )
	follower.x = startPoint.x
	follower.y = startPoint.y

	follower.rotation = angleBetween( pathPoints[1].x, pathPoints[1].y, pathPoints[2].x, pathPoints[2].y )

	--add follower to module for reference
	M.obj = follower

	if params.parent then
		params.parent:insert(follower)
	end
	
	local precision = pathPrecision
	if ( pathPrecision == 0 ) then
		precision = distBetween( pathPoints[1].x, pathPoints[1].y, pathPoints[2].x, pathPoints[2].y )
	end
	
	--if "showPoints" is true, plot points along path
	if ( params.showPoints == true ) then
		local pathPointsGroup = display.newGroup() ; pathPointsGroup:toBack()
		for p = 1,#pathPoints do
			local dot = display.newCircle( pathPointsGroup, 0, 0, 8 )
			dot:setFillColor( 1, 1, 1, 0.4 )
			dot.x = pathPoints[p].x
			dot.y = pathPoints[p].y
		end
		M.ppg = pathPointsGroup
	end

	follow( params, follower, pathPoints, precision )
	
end


return M
