
-- Abstract: Curve Drawing
--
-- Version: 1.0
--
-- Sample code is MIT licensed, see http://www.coronalabs.com/links/code/license
-- Copyright (C) 2014 Corona Labs Inc. All Rights Reserved.
------------------------------------------------------------

local Curve = {}

--adjust this number to effect the "smoothness" of the curve (total number of segments)
--print("require curve")
function Curve.getCurve( anchorPoints, segNum)
    --print("render curve")
    local pathPoints = {}

    pathPoints[1] = {
        x = anchorPoints[1].x,
        y = anchorPoints[1].y
    }

    pathPoints.anchorPoints = anchorPoints

    for j = 1,segNum do
        pathPoints[#pathPoints+1] = { x=0, y=0, t=0 }
    end



    for i = 1,#anchorPoints do
--        print("Anchor point "..i..": "..anchorPoints[i].x.." ,"..anchorPoints[i].y)
    end
    local inc = ( 1.0 / segNum )

    for i = 1,#anchorPoints,4 do
--        print("render cuve "..i)
        local t = 0
        local t1 = 0
        local i = 1

        for j = 2, segNum + 1 do
            t1 = 1.0 - t
            local t1_3 = t1 * t1 * t1
            local t1_3a = (3*t) * (t1*t1)
            local t1_3b = (3*(t*t)) * t1
            local t1_3c = t * t * t
            local p1 = anchorPoints[i]
            local p2 = anchorPoints[i+1]
            local p3 = anchorPoints[i+2]
            local p4 = anchorPoints[i+3]
    
            local x = t1_3 * p1.x
            x = x + t1_3a * p2.x
            x = x + t1_3b * p3.x
            x = x + t1_3c * p4.x

            local y = t1_3 * p1.y
            y = y + t1_3a * p2.y
            y = y + t1_3b * p3.y
            y = y + t1_3c * p4.y

            pathPoints[j].x = x
            pathPoints[j].y = y
            pathPoints[j].t = t
            t = t + inc
        end

        pathPoints[#pathPoints+1] = {
            x = anchorPoints[4].x,
            y = anchorPoints[4].y
        }
    end

    function pathPoints:split(t)
        local x1, y1 = self.anchorPoints[1].x, self.anchorPoints[1].y
        local x2, y2 = self.anchorPoints[2].x, self.anchorPoints[2].y
        local x3, y3 = self.anchorPoints[3].x, self.anchorPoints[3].y
        local x4, y4 = self.anchorPoints[4].x, self.anchorPoints[4].y

        local x12 = (x2-x1)*t+x1
        local y12 = (y2-y1)*t+y1

        local x23 = (x3-x2)*t+x2
        local y23 = (y3-y2)*t+y2

        local x34 = (x4-x3)*t+x3
        local y34 = (y4-y3)*t+y3

        local x123 = (x23-x12)*t+x12
        local y123 = (y23-y12)*t+y12

        local x234 = (x34-x23)*t+x23
        local y234 = (y34-y23)*t+y23

        local x1234 = (x234-x123)*t+x123
        local y1234 = (y234-y123)*t+y123

        return {
            {
                {
                    x = x1, y = y1
                },
                {
                    x= x12, y = y12
                },
                {
                    x= x123, y = y123
                },
                {
                    x = x1234, y = y1234
                }
            },
            {
                {
                    x = x1234, y = y1234
                },
                {
                    x= x234, y = y234
                },
                {
                    x= x34, y = y34
                },
                {
                    x = x4, y = y4
                }
            },

        }
    end

    return pathPoints
end

return Curve
