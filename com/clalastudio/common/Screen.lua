local Screen = {}

function Screen:init(options)

    self.cw = display.contentWidth
    self.ch = display.contentHeight
    self.pw = display.pixelWidth
    self.ph = display.pixelHeight
    self.scaleW = self.pw / self.cw
    self.scaleH =  self.ch / self.ch
    self.scale = 1
    self.suffix = display.imageSuffix

    local cRatio = self.cw / self.ch
    local rRatio = self.pw / self.ph

    if cRatio > rRatio then
        self.l = 0
        self.r = self.cw
        self.t = (self.ch - self.ph * (self.cw / self.pw)) / 2
        self.b = (self.ch + self.ph * (self.cw / self.pw)) / 2
    elseif cRatio / self.ch < rRatio then
        self.l = (self.cw - self.pw * (self.ch / self.ph)) / 2
        self.r = (self.cw + self.pw * (self.ch / self.ph)) / 2
        self.t = 0
        self.b = self.ch
    else
        self.l = 0
        self.r = self.cw
        self.t = 0
        self.b = self.ch
    end 
    self.h = self.b - self.t
    self.w = self.r - self.l
    self.centerX = (self.cw)/2
    self.centerY = (self.ch)/2
    self.contentX = 0
    self.contentY = 0
    self.mapR = self.r + 20
    self.mapL = self.l - 20
end

function Screen:dispatchMapMoveEvent(offsetX, offsetY)
end

function Screen:requestMapMove(offsetX, offsetY)
    local rightRemain = Screen.mapR - Screen.r
    local leftRemain = Screen.l - Screen.mapL
    local mapOffsetX = 0
    local newOffsetX = offsetX
    local newOffsetY = offsetY
    local mapOffsetX = 0
    local mapOffsetY = 0

    if offsetX > 0 and Screen.mapR > Screen.r then
        if math.abs(offsetX) < rightRemain then
            newOffsetX = 0
            mapOffsetX = - offsetX
        else
            mapOffsetX = rightRemain
            newOffsetX = offsetX - rightRemain
        end
    elseif offsetX < 0 and Screen.mapL < Screen.l then
        if math.abs(offsetX) < leftRemain then
            newOffsetX = 0
            mapOffsetX = - offsetX
        else
            mapOffsetX = - leftRemain
            newOffsetX = offsetX + leftRemain
        end
    end

    Screen.mapR = Screen.mapR + mapOffsetX
    Screen.mapL = Screen.mapL + mapOffsetX

    self:dispatchMapMoveEvent(mapOffsetX, mapOffsetY)

    return newOffsetX, newOffsetY
end

function Screen:printInfo()
    print("[ScreenInfo] cw:", self.cw, ", ch:", self.ch, ", pw:", self.pw, ", ph:", self.ph, ", l:", self.l, ", r:", self.r, ", t:", self.t, ", b:", self.b, ", suffix: ", self.suffix)
end

Screen:init()

Screen:printInfo()

return Screen