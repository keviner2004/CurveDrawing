--logging
local Logging = require("com.clalastudio.common.Logging")
local logger = Logging.new()
logger:setLevel(Logging.VERBOSE)
return logger