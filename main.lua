
-- Abstract: Curve Drawing
--
-- Version: 1.0
--
-- Sample code is MIT licensed, see http://www.coronalabs.com/links/code/license
-- Copyright (C) 2014 Corona Labs Inc. All Rights Reserved.
------------------------------------------------------------
require("override")
local logger = require("com.clalastudio.common.logger")
local Button = require("Button")
local widget = require("widget")
local Curve = require("curve")
local Screen = require("com.clalastudio.common.Screen")

local TOOL_DRAG = 1
local TOOL_EXTEND = 2
local json = require( "json" )

local hud = nil

local root = nil

local currentTool = TOOL_DRAG

display.setStatusBar( display.HiddenStatusBar )

--require "follow.lua" module (remove if using only main draw code)
local followModule = require( "follow" )
--declare follow module parameters (remove if using only main draw code)
local CurveObject = {}

local function isExists(obj)
    if not obj then
        return false
    end
    if obj.x == nil or obj.parent == nil then
        return false
    else
        return true
    end
end

local curves = {}

local Canvas = {}

Canvas.new = function()

    local obj = display.newGroup()

    obj.vWidth = 360
    obj.vHight = 640

    obj.x = (Screen.cw - obj.vWidth)/2
    obj.y = (Screen.ch - obj.vHight)/2
    obj.l = obj.x
    obj.r = obj.x + obj.vWidth
    obj.t = obj.y + obj.vHight

    local lineT = display.newLine( 0, 0, obj.vWidth, 0)
    local lineB = display.newLine( 0, obj.vHight, obj.vWidth, obj.vHight )
    local lineL = display.newLine( 0, 0, 0, obj.vHight )
    local lineR = display.newLine( obj.vWidth, 0, obj.vWidth, obj.vHight )
    print("Create canvas: ", obj.x, obj.y)
    lineT.fill = {0, 0, 1}
    lineB.fill = {0, 0, 1}
    lineL.fill = {0, 0, 1}
    lineR.fill = {0, 0, 1}
    lineT.strokeWidth = 2
    lineL.strokeWidth = 2
    lineB.strokeWidth = 2
    lineR.strokeWidth = 2

    obj:insert(lineT)
    obj:insert(lineB)
    obj:insert(lineL)
    obj:insert(lineR)

    return obj

end

local canvas = Canvas.new()

local followParams = { parent = canvas, segmentTime=1, constantRate=true, showPoints=false, speed = 300 }

CurveObject.new = function(options)

    local obj = display.newGroup()

    if options and options.parent then
        options.parent:insert(obj)
    end

    obj.curve = nil
    obj.pathPoints = {}
    obj.anchorCount = 0
    obj.anchorPoints = (options and options.anchorPoints) or { {},{},{},{} }
    obj.curveHandleLines = {}
    obj.curveSegments = (options and options.curveSegments) or 100

    for j = 1, obj.curveSegments do
        obj.pathPoints[#obj.pathPoints+1] = { x=0, y=0 }
    end

    function obj:init()

    end

    function obj:renderHandles( endX, endY, startAnchor, endAnchor, handle )

        --move handle anchor in unison with touch
        if endX then
            endAnchor.x = endX
        end
        if endY then
            endAnchor.y = endY
        end

        local thisHandle
        if ( handle == "first" ) then
            --if a handle line already exists, remove it so it can be replaced
            if ( self.curveHandleLines[1] ) then display.remove( self.curveHandleLines[1] ) end
            --render handle line
            self.curveHandleLines[1] = display.newLine( startAnchor.x, startAnchor.y, endAnchor.x, endAnchor.y )
            self:insert(self.curveHandleLines[1])
            thisHandle = self.curveHandleLines[1]
        elseif ( handle == "second" ) then
            --if a handle line already exists, remove it so it can be replaced
            if ( self.curveHandleLines[2] ) then display.remove( self.curveHandleLines[2] ) end
            --render handle line
            self.curveHandleLines[2] = display.newLine( startAnchor.x, startAnchor.y, endAnchor.x, endAnchor.y )
            thisHandle = self.curveHandleLines[2]
            self:insert(self.curveHandleLines[2])
        end
        thisHandle:setStrokeColor( 1, 1, 1, 0.5 )
        thisHandle.strokeWidth = 4
        thisHandle:toBack()

    end

    function obj:hasAnchor(pos)
        if isExists(self.anchorPoints[pos]) then
            return true
        end
    end

    function obj:renderAllHandles()
        --on touch move, render/re-render handles and curve line if applicable
        if self:hasAnchor(2) then
            self:renderHandles( nil, nil, self.anchorPoints[1], self.anchorPoints[2], "first" )
        end
        if self:hasAnchor(4) then
            self:renderHandles( nil, nil, self.anchorPoints[4], self.anchorPoints[3], "second" )
            --(re)render curve
            self:renderCurve(1, 1, 1 )
        end

    end

    function obj:clear()
        self:removeSelf()
    end

    function obj:renderCurve(r, g, b )
        local segNum = self.curveSegments
        -- print("render curve:")
        for i = 1,#self.anchorPoints do
            -- print("Anchor point "..i..": "..self.anchorPoints[i].x.." ,"..self.anchorPoints[i].y)
        end
        self.pathPoints = Curve.getCurve(
            self.anchorPoints
            , segNum)

        --reset/clear core
        if self.curve then display.remove( self.curve ) end

        -- curve = display.newLine( pathPoints[1].x, pathPoints[1].y, pathPoints[2].x, pathPoints[2].y )

        self.curve = display.newGroup()

        self:insert(self.curve)

        self.curve.line = display.newLine( self.pathPoints[1].x, self.pathPoints[1].y, self.pathPoints[2].x, self.pathPoints[2].y )

        self.curve:insert(self.curve.line)
        for i = 1,#self.pathPoints do
            if i >= 3 then
                self.curve.line:append( self.pathPoints[i].x, self.pathPoints[i].y )
            end

            local pointObj = display.newCircle(0, 0, 10)
            pointObj.x = self.pathPoints[i].x
            pointObj.y = self.pathPoints[i].y
            pointObj.t = self.pathPoints[i].t
            pointObj.fill = { r, g, b }
            pointObj:addEventListener("touch", function(event)
                if ( event.phase == "ended" ) then
                    -- Code executed when the touch lifts off the object
                    print( "touch ended on object ", pointObj.t )
                    print( "split line: " .. tostring(event.target) )
                    if pointObj.t then
                        -- split by t, unchanged point for part1 is 1, for part2 is4
                        local splitResult = self.pathPoints:split(pointObj.t)

                        local conncectedMap = {}

                        local fixedAnchor1 = self.anchorPoints[1]
                        local fixedAnchor2 = self.anchorPoints[4]

                        --fixedAnchor1:clearLinsters()
                        --fixedAnchor1:clearLinsters()

                        print("Split result:")
                        table.print(splitResult)
                        local subCurveObjs = {}
                        for i = 1, #splitResult do
                            local subCurveObj = CurveObject.new({
                                parent = canvas
                            })
                            for j = 1, #splitResult[i] do
                                local _p = splitResult[i][j]
                                subCurveObj:addAnchor(j, _p.x, _p.y)
                            end
                            subCurveObjs[#subCurveObjs + 1] = subCurveObj
                        end

                        subCurveObjs[1]:replaceAnchor(1, fixedAnchor1, {
                            reInsert = true
                        })

                        fixedAnchor1.owner = subCurveObjs[1]

                        subCurveObjs[2]:replaceAnchor(4, fixedAnchor2, {
                            reInsert = true
                        })

                        fixedAnchor2.owner = subCurveObjs[2]

                        subCurveObjs[1]:replaceAnchor(4, subCurveObjs[2].anchorPoints[1], {
                            reInsert = false
                        })

                        if root == self then
                            root = subCurveObjs[1]
                        end

                        subCurveObjs[1].parentCurve = self.parentCurve
                        subCurveObjs[1].childCurve = subCurveObjs[2]
                        subCurveObjs[2].parentCurve = subCurveObjs[1]
                        subCurveObjs[2].childCurve = self.childCurve
                        for i = 1, #subCurveObjs do
                            subCurveObjs[i]:addDragHandles()
                            subCurveObjs[i]:renderAllHandles()
                        end

                        self:clear()

                    end
                end
                return true
            end)
            self.curve:insert( pointObj )

        end
        self.curve.line:append( self.anchorPoints[4].x, self.anchorPoints[4].y )
        self.curve.line:setStrokeColor( 1, 1, 1 )
        self.curve.line.strokeWidth = 10

        -- curve:append( anchorPoints[4].x, anchorPoints[4].y )
        self.curve:toBack()

    end

    function obj:dragHandles( event )

        local t = event.target
        local x, y = canvas:contentToLocal(event.x, event.y)

        if ( event.phase == "began" ) then

            display.currentStage:setFocus( t )
            t.isFocus = true
            if currentTool == TOOL_DRAG then
                print("drag curve")
                --reset/clear follow module items (remove these lines if not using "follow.lua")
                transition.cancel( "moveObject" )
                if ( followModule.obj ) then display.remove( followModule.obj ) ; followModule.obj = nil end
                if ( followModule.ppg ) then
                    for p = followModule.ppg.numChildren,1,-1 do display.remove( followModule.ppg[p] ) end
                end
            elseif currentTool == TOOL_EXTEND then
                if t:isMainPoint() and not t.connectedCurve then
                    print("extend curve. create and connected a new one", t.pos)
                    self.newCurve = self:connect(t)
                    t.b = self.newCurve
                else
                    print("extend curve fail.", t.pos)
                end

            end

        elseif ( t.isFocus ) then

            if ( event.phase == "moved" ) then

                if currentTool == TOOL_DRAG then
                    self:renderCurve(1, 1, 1 )

                    if ( t == self.anchorPoints[1] ) then
                        self:renderHandles( x, y, self.anchorPoints[2], self.anchorPoints[1], "first" )
                    elseif ( t == self.anchorPoints[2] ) then
                        self:renderHandles( x, y, self.anchorPoints[1], self.anchorPoints[2], "first" )
                    elseif ( t == self.anchorPoints[3] ) then
                        self:renderHandles( x, y, self.anchorPoints[4], self.anchorPoints[3], "second" )
                    elseif ( t == self.anchorPoints[4] ) then
                        self:renderHandles( x, y, self.anchorPoints[3], self.anchorPoints[4], "second" )
                    end
                elseif currentTool == TOOL_EXTEND and self.newCurve then

                    self.newCurve:renderHandles( x, y, self.newCurve.anchorPoints[3], self.newCurve.anchorPoints[4], "second" )

                end


            elseif ( event.phase == "ended" or event.phase == "cancelled" ) then

                display.currentStage:setFocus( nil )
                t.isFocus = false
                self.newCurve = nil
                if currentTool == TOOL_EXTEND then
                    hud:selectTool(TOOL_DRAG)
                elseif currentTool == TOOL_DRAG then
                    self:renderCurve(1, 0.5, 0 )

                    --start follow module
                    if ( #self.pathPoints > 1 ) then
                        followModule.init( followParams, self.pathPoints, 0, self.anchorPoints[1] )
                    end
                end
            end
        end

        return true
    end

    function obj:addDragHandles()
        --add touch listener to each handle
        for i = 1,#self.anchorPoints do
            local dragHandler = nil
            dragHandler = function(event)
                if not isExists(self) then
                    self.anchorPoints[i]:removeEventListener("touch", dragHandler)
                end

                return self:dragHandles(event)
            end

            self.anchorPoints[i]:addEventListener( "touch", dragHandler)
        end
    end

    function obj:replaceAnchor(pos, anchor, options)
        if self.anchorPoints[pos].removeSelf then
            self.anchorPoints[pos]:removeSelf()
        end
        self.anchorPoints[pos] = anchor
        anchor.connected = true
        if options.reInsert then
            self:insert(anchor)
        end
    end

    function obj:connect(anchor)
        local subCurveObj = CurveObject.new({
            parent = canvas
        })

        subCurveObj:addAnchor(1, anchor.x, anchor.y)
        subCurveObj:addAnchor(2, anchor.x, anchor.y)
        subCurveObj:addAnchor(3, anchor.x, anchor.y)
        subCurveObj:addAnchor(4, anchor.x, anchor.y)

        subCurveObj:replaceAnchor(1, anchor, {
            reInsert = false
        })
        subCurveObj:addDragHandles()
        subCurveObj:renderAllHandles()
        subCurveObj:toBack()
        anchor.owner.childCurve = subCurveObj
        return subCurveObj
    end

    function obj:addAnchor(pos, x, y)
        local point = display.newCircle( x, y, 20 )
        point.pos = pos
        point.owner = self
        function point:isMainPoint()
            if self.pos == 1 or self.pos == 4 then
                return true
            end
            return false
        end

        if ( pos == 1 ) then
            point:setFillColor( 0.2, 0.8, 0.4 )
        elseif ( pos == 4 ) then
            point:setFillColor( 1, 0, 0.2 )
        elseif ( pos == 3 ) then
            point:setFillColor( 0.4 )
            point:toBack()
        elseif ( pos == 2 ) then
            point:setFillColor( 0.4 )
            point:toBack()
        end

        self.anchorPoints[pos] = point

        self:insert(point)
    end


    return obj

end


local anchorCount = 0
local curveSegments = 100

local curveObj = nil

function initProgram()
    anchorCount = 0
    curveSegments = 100

    curveObj = CurveObject.new({
        parent = canvas
    })

    root = curveObj
end

initProgram()

local function addCurveAnchors( event )
    local x, y = canvas:contentToLocal(event.x, event.y)
    --if both points and both control points have not yet been placed, proceed with routine
    if ( anchorCount <= 4 ) then

        if ( event.phase == "began" ) then
            display.currentStage:setFocus( event.target )
            anchorCount = anchorCount+1

            if ( anchorCount == 1 ) then
                curveObj:addAnchor(1, x, y)
            elseif ( anchorCount == 3 ) then
                curveObj:addAnchor(4, x, y)
            end

            anchorCount = anchorCount+1
            if ( anchorCount == 2 ) then
                curveObj:addAnchor(2, x, y)
            elseif ( anchorCount == 4 ) then
                curveObj:addAnchor(3, x, y)
                curveObj:renderCurve(1, 0.5, 0 )
            end

        elseif ( event.phase == "moved" ) then

            --on touch move, render/re-render handles and curve line if applicable
            if ( anchorCount == 2 ) then
                curveObj:renderHandles( x, y, curveObj.anchorPoints[1], curveObj.anchorPoints[2], "first" )
            elseif ( anchorCount == 4 ) then
                curveObj:renderHandles( x, y, curveObj.anchorPoints[4], curveObj.anchorPoints[3], "second" )
                --(re)render curve
                curveObj:renderCurve(1, 1, 1 )
            end

        elseif ( event.phase == "ended" or event.phase == "cancelled" ) then

            display.currentStage:setFocus( nil )
            event.target.isFocus = false

            if ( anchorCount == 4 ) then
                display.currentStage:removeEventListener( "touch", addCurveAnchors )

                --reset/clear follow module items (remove these lines if not using "follow.lua")
                transition.cancel( "moveObject" )
                if ( followModule.obj ) then display.remove( followModule.obj ) ; followModule.obj = nil end
                if ( followModule.ppg ) then
                    for p = followModule.ppg.numChildren,1,-1 do display.remove( followModule.ppg[p] ) end
                end

                curveObj:addDragHandles()

                --(re)render curve
                curveObj:renderCurve(1, 0.5, 0 )

                --start follow module
                if ( #curveObj.pathPoints > 1 ) then
                    followModule.init( followParams, curveObj.pathPoints, 0, curveObj.anchorPoints[1] )
                end

            end

        end
    end

    return true
end

local textInputGroup = nil

local function createCurvesByJson(jsonStr)
    --[[
    for i = 1, canvas.numChildren do
        canvas[1]:removeSelf()
    end
    --]]

    canvas = Canvas.new()

    initProgram()
    print("Canvas created: ", jsonStr)

    local curveConfig = json.decode(jsonStr)

    local initCurve = false

    -- table.print(curveConfig)

    local preCurve = nil

    local curves = {}

    for i = 1, #curveConfig do 
        local cc = curveConfig[i]
        local target = nil
        if not initCurve then
            curveObj:addAnchor(1, cc[1].x, cc[1].y)
            curveObj:addAnchor(2, cc[2].x, cc[2].y)
            curveObj:addAnchor(3, cc[3].x, cc[3].y)
            curveObj:addAnchor(4, cc[4].x, cc[4].y)
            -- curveObj:toBack()
            target = curveObj
            initCurve = true  
        else
            local subCurveObj = CurveObject.new({
                parent = canvas
            })

            subCurveObj:addAnchor(1, cc[1].x, cc[1].y)
            subCurveObj:addAnchor(2, cc[2].x, cc[2].y)
            subCurveObj:addAnchor(3, cc[3].x, cc[3].y)
            subCurveObj:addAnchor(4, cc[4].x, cc[4].y)
            -- subCurveObj:toBack()
            target = subCurveObj
        end

        curves[#curves + 1] = target

        if preCurve then
            preCurve.childCurve = target
            target.parentCurve = preCurve

            preCurve:replaceAnchor(4, target.anchorPoints[1], {
                reInsert = false
            })

        end


        preCurve = target
    end

    for i = 1, #curves do 
        local target = curves[i]
        target:addDragHandles()
        target:renderAllHandles()
    end
end

local function import()
    if not textInputGroup then
        textInputGroup = display.newGroup()
    end

    local textInput = native.newTextBox( Screen.centerX, Screen.centerY, 400, 600 )
    textInput.isEditable = true

    local importBtn = Button.new({
        shape = "rect",
        width = 100,
        labelColor = { default={ 1, 0, 0 }, over={ 0, 1, 0 } },
        fillColor = { default={ 0, 1, 0 }, over={ 1, 0, 0 } },
        label = "Import"
    })


    textInputGroup:insert(textInput)
    textInputGroup:insert(importBtn)

    importBtn:addEventListener("clicked", 
        function (event)
            print("Close textInputGroup")
            textInputGroup:removeSelf()
            textInputGroup = nil
            createCurvesByJson(textInput.text)
        end
    )

    importBtn.x = Screen.centerX
    importBtn.y = Screen.centerY + 400 + importBtn.height / 2
end

local function export()
    local exCurve = root
    local _root = root
    -- find root
    while true do
        local parent = exCurve.parentCurve
        if not parent then
            _root = exCurve
            break
        end
        exCurve = parent
    end

    local exCurve = {
        childCurve = _root
    }

    local jsons = {}
    print("Export :")
    -- table.print(root)
    while true do
        local child = exCurve.childCurve
        if not child then
            break
        end
        exCurve = child

        local anchorJson = {}
        jsons[#jsons + 1] = anchorJson

        for i = 1, #child.anchorPoints do
            local ach = child.anchorPoints[i]
            anchorJson[#anchorJson +1] = {
                x = ach.x,
                y = ach.y
            }
        end
    end

    local encoded = json.encode( jsons, { indent=true } )
    print(encoded)
end

local function initHUD()

    print("initHUD")
    local hud = display.newGroup()
    local tools = display.newGroup()

    local function turnOffOtherTools(tool)
        for i = 1, tools.numChildren do
            if tool~= tools[i] then
                tools[i]:setStatus(Button.STATUS_DEFAULT)
            end
        end
    end

    local function toggleTools(tool)
        tool:setStatus(Button.STATUS_CLICKED)
        turnOffOtherTools(tool)
    end

    local function registerToolBoxBtn(btn, options)
        btn:addEventListener("clicked", function(event)
            local _t = event.target
            print("Turn off other tool buttons")
            turnOffOtherTools(_t)
            if options.onClick then
                options.onClick()
            end
        end)
    end

    local function registerExportBoxBtn(btn, options)
        btn:addEventListener("clicked", function(event)
            export()
        end)
    end

    local function registerImportBoxBtn(btn, options)
        btn:addEventListener("clicked", function(event)
            import()
        end)
    end

    display.newLine( 0, 0, Screen.cw, 0 )
    display.newLine( 0, Screen.ch, Screen.cw, Screen.ch )
    display.newLine( 0, 0, 0, Screen.ch )
    display.newLine( Screen.cw, 0, Screen.cw, Screen.ch )

    local exportBtn = Button.new({
        shape = "rect",
        width = 50,
        labelColor = { default={ 1, 0, 0 }, over={ 0, 1, 0 } },
        fillColor = { default={ 0, 1, 0 }, over={ 1, 0, 0 } },
        label = "Export"
    })

    local importBtn = Button.new({
        shape = "rect",
        width = 50,
        labelColor = { default={ 1, 0, 0 }, over={ 0, 1, 0 } },
        fillColor = { default={ 0, 1, 0 }, over={ 1, 0, 0 } },
        label = "Import"
    })

        -- drag button
    local dragToolBtn = Button.new({
        shape = "rect",
        width = 50,
        labelColor = { default={ 1, 0, 0 }, over={ 0, 1, 0 } },
        fillColor = { default={ 0, 1, 0 }, over={ 1, 0, 0 } },
        label = "Drag",
        toggled = true
    })

    dragToolBtn.x = Screen.r - dragToolBtn.width / 2 - 20
    dragToolBtn.y = Screen.t + dragToolBtn.height / 2 + 20
    registerToolBoxBtn(dragToolBtn, {
        onClick = function()
            currentTool = TOOL_DRAG
        end
    })

    exportBtn.x = Screen.l + exportBtn.width / 2 + 20
    exportBtn.y = Screen.t + exportBtn.height / 2 + 20
    registerExportBoxBtn(exportBtn)

    importBtn.x = exportBtn.x + importBtn.width / 2 + 20 * 2
    importBtn.y = exportBtn.y
    registerImportBoxBtn(importBtn)

    -- extend button
    local extendToolBtn = Button.new({
        shape = "rect",
        width = 50,
        labelColor = { default={ 1, 0, 0 }, over={ 0, 1, 0 } },
        fillColor = { default={ 0, 1, 0 }, over={ 1, 0, 0 } },
        label = "Drag",
        toggled = true
    })

    extendToolBtn:setLabel("Ext")
    extendToolBtn.x = dragToolBtn.x - dragToolBtn.width / 2 - 20 * 2
    extendToolBtn.y = dragToolBtn.y
    registerToolBoxBtn(extendToolBtn, {
        onClick = function()
            currentTool = TOOL_EXTEND
        end
    })

    toggleTools(dragToolBtn)

    tools:insert(dragToolBtn)
    tools:insert(extendToolBtn)
    hud:insert(tools)


    local toolMap = {}

    toolMap[TOOL_DRAG] = dragToolBtn
    toolMap[TOOL_EXTEND] = extendToolBtn

    function hud:selectTool(toolIdx)
        local tool = toolMap[toolIdx]
        tool:fakeClick()
    end

    return hud
end

hud = initHUD()

display.currentStage:addEventListener( "touch", addCurveAnchors )

