local Button = {}
local TAG = "Button"
local Screen = require("com.clalastudio.common.Screen")
local logger = require("com.clalastudio.common.logger")

Button.STATUS_CLICKED = 1
Button.STATUS_DEFAULT = 2
Button.STATUS_HOVERED = 3

Button.new = function(options)
    local button = display.newGroup()

    button.contentGroup = display.newGroup()
    button:insert(button.contentGroup)

    if options and options.content then
        button:insert(options.content)
        button.content = options.content
    end

    -- static bg
    -- default
    local defaultStaticBg = display.newRect(
        0,
        0,
        (options and options.width) or 100,
        (options and options.height) or 20
    )
    button.defaultStaticBg = defaultStaticBg
    button.toggled = options.toggled or false
    button.labelGroup = display.newGroup()
    button:insert(button.labelGroup)
    button.contentGroup:insert(defaultStaticBg)

    -- over
    local overStaticBg = display.newRect(
        0,
        0,
        (options and options.width) or 100,
        (options and options.height) or 20
    )
    button.overStaticBg = overStaticBg
    button.contentGroup:insert(overStaticBg)

    button.defaultLabel = display.newText({
        text = ""
    })
    button.labelGroup:insert(button.defaultLabel)

    button.overLabel = display.newText({
        text = ""
    })
    button.labelGroup:insert(button.overLabel)

    button.pressFx = (options and options.pressFx) or "shrink"

    function button:click(event)
        logger:debug(TAG, "Button was clicked")
        button:dispatchEvent({
            name = "clicked",
            target = button
        })
    end

    function button:setFillColor(fillColor)
        self.defaultStaticBg.fill = fillColor.default
        self.overStaticBg.fill = fillColor.over
    end

    function button:setLabel(text)
        self.defaultLabel.text = text
        self.overLabel.text = text
    end

    function button:setLabelColor(fillColor)
        self.defaultLabel.fill = fillColor.default
        self.overLabel.fill = fillColor.over
    end


    function button:pressEffect()
        if self.pressFx == "shrink" then
            transition.to(self, {
                time = 100,
                xScale = 0.98,
                ySscale = 0.98
            })
        end
    end

    function button:unPressEffect()
        if self.pressFx == "shrink" then
            transition.to(self, {
                time = 100,
                xScale = 1,
                ySscale = 1
            })
        end
    end

    function button:setStatus(status)
        print("setStatus: ", status)
        if status == Button.STATUS_DEFAULT then
            self.defaultLabel.alpha = 1
            self.defaultStaticBg.alpha = 1
            self.overLabel.alpha = 0
            self.overStaticBg.alpha = 0
            self.overLabel:toBack()
            self.overStaticBg:toBack()
            self:unPressEffect()
        elseif status == Button.STATUS_CLICKED then
            self.defaultLabel.alpha = 0
            self.defaultStaticBg.alpha = 0
            self.overLabel.alpha = 1
            self.overStaticBg.alpha = 1
            self.defaultLabel:toBack()
            self.defaultStaticBg:toBack()
            self:pressEffect()
        end
        self.status = status
    end

    if options and options.label then
        button:setLabel(options.label)
    end

    if options and options.fillColor then
        button:setFillColor(options.fillColor)
    else
        button:setFillColor({1, 1, 1, 0})
    end

    if options and options.labelColor then
        button:setLabelColor(options.labelColor)
    end

    button:setStatus(Button.STATUS_DEFAULT)

    function button:fakeClick()
        button:touch({
            phase = "began",
            passClickCheck = true,
            x = 0,
            y = 0
        })
        button:touch({
            phase = "ended",
            passClickCheck = true,
            x = 0,
            y = 0
        })
    end

    function button:touch(event)
        local tx, ty = self:contentToLocal(event.x, event.y)

        --[[
        if event.x > Screen.r or event.x < Screen.l then
          display.getCurrentStage():setFocus(nil)
        end

        if event.y > Screen.t or event.y < Screen.b then
          display.getCurrentStage():setFocus(nil)
        end
        --]]

        if event.phase == "began" then
            display.getCurrentStage():setFocus(event.target)
            if options.toggled then
                button:click(event)
                if self.status == Button.STATUS_CLICKED then
                    self:setStatus(Button.STATUS_DEFAULT)
                else
                    self:setStatus(Button.STATUS_CLICKED)
                end
            else
                self:setStatus(Button.STATUS_CLICKED)
            end
        elseif event.phase == "moved" then

        elseif event.phase == "ended" or event.phase == "canceled" then
            display.getCurrentStage():setFocus(nil)
            if options.toggled then
                return true
            end
            self:setStatus(Button.STATUS_DEFAULT)
            if not event.passClickCheck then
                if tx < self.contentWidth / 2 and
                        tx > -self.contentWidth / 2 and
                        ty < self.contentHeight / 2 and
                        ty > -self.contentHeight / 2 then
                    button:click(event)
                end
            end
        end
        return true
    end

    button:addEventListener("touch", button)

    return button
end

return Button
